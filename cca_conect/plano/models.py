from django.db import models
from django.utils.translation import gettext_lazy as _


class Plano (models.Model):

    nome_plano = models.CharField (_('Nome do Plano'), max_length=255)
    preco_plano = models.DecimalField(_('Preço do Plano'), max_digits=5, decimal_places=2)
    duracao_plano = models.IntegerField(_('Duração do Plano'),)

    def __str__(self):
        return self.nome_plano
    
    class Meta: 
        db_table='plano'

