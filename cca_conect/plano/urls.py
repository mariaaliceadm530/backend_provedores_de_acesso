from django.urls import path
from .views import *

urlpatterns = [
    path ('', PlanoListar.as_view()),
    path ('<int:pk>/', PlanoDetalhar.as_view()),
    path ('cadastrar/', PlanoCadastrar.as_view()),
    path ('atualizar/<int:pk>/', PlanoAtualizar.as_view()),
    path ('deletar/<int:pk>/', PlanoDeletar.as_view()),


]
