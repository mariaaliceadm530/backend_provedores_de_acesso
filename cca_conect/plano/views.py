from rest_framework import generics, status, response
from .models import Plano
from .serializers import PlanoSerializer


class PlanoListar (generics.ListAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializer

class PlanoDetalhar (generics.RetrieveAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializer
  
class PlanoCadastrar (generics.CreateAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializer

class PlanoAtualizar (generics.RetrieveUpdateAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializer

class PlanoDeletar (generics.RetrieveDestroyAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializer






