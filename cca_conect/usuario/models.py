from django.db import models
from django.utils.translation import gettext_lazy as _

class usuario (models.Model):
    nome_usuario = models.CharField (_('Nome do Usuário'), max_length=255)
    email_usuario = models.EmailField (_('Email do Usuário'), max_length=255, unique=True)
    senha_usuario = models.IntegerField (_('Senha do Usuário'),)
    numero_usuario = models.CharField(_('Número do Usuário'), max_length=15)
    cpf_usuario = models.CharField(_('CPF do Usuário'), max_length=14, unique=True)

    def __str__(self):
        return self.nome_usuario
    
    class Meta:
        db_table='usuario'

    




