from django.urls import path
from .views import *

urlpatterns = [

path ('', UsuarioListar.as_view()),
path ('<int:pk>/', UsuarioDetalhar.as_view()),
path ('cadastrar/', UsuarioCadastrar.as_view()),
path ('atualizar/<int:pk>/', UsuarioAtualizar.as_view()),
path ('deletar/<int:pk>/', UsuarioDeletar.as_view()),

]