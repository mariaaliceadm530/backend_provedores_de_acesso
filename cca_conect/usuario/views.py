from rest_framework import generics, status, response
from .models import usuario 
from .serializers import UsuarioSerializers

class UsuarioListar (generics.ListCreateAPIView):
    queryset = usuario.objects.all ()
    serializer_class = UsuarioSerializers

class UsuarioDetalhar (generics.RetrieveAPIView):
    queryset = usuario.objects.all ()
    serializer_class = UsuarioSerializers

class UsuarioCadastrar (generics.CreateAPIView):
    queryset = usuario.objects.all ()
    serializer_class = UsuarioSerializers  

class UsuarioAtualizar (generics.RetrieveUpdateAPIView):
    queryset = usuario.objects.all ()
    serializer_class = UsuarioSerializers

class UsuarioDeletar (generics.RetrieveDestroyAPIView):
    queryset = usuario.objects.all ()
    serializer_class = UsuarioSerializers

    






